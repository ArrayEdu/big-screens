import http from '../tools/http.js'

/**
 *  @parms resquest 请求地址 例如：http://127.0.0.1:8085/api/...
 *  @param '/api'代表vue-cil中config，vue-store.js中配置的代理
 */
let resquest = "/api";

// get请求
export function getListAPI(params){
    return http.get(`/index/date`,params)
}
// post请求
export function postFormAPI(params){
    return http.post(`${resquest}/postForm.json`,params)
}
// put 请求
export function putSomeAPI(params){
    return http.put(`${resquest}/putSome.json`,params)
}
// delete 请求
export function deleteListAPI(params){
    return http.delete(`${resquest}/deleteList.json`,params)
}
