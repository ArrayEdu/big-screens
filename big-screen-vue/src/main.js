import Vue from "vue";
import App from "./App.vue";
import router from "./router/router.js";
import store from "./store/vue-store.js";
import dataV from "@jiaminghi/data-view";
// import axios from "axios";

Vue.use(dataV);

// 引入vue-awesome
import Icon from "vue-awesome/components/Icon";
import "vue-awesome/icons/index.js";

// 全局注册图标
Vue.component("icon", Icon);

// 适配flex
import "@/common/flexible.js";

// 引入全局css
import "./assets/scss/style.scss";

// 自定义浏览器页面title
router.beforeEach((to, from, next) => {
	window.document.title = to.meta.title == undefined ? 'big-screen' : to.meta.title
	next();
});

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount("#app");
