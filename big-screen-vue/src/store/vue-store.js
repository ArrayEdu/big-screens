import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {},
	mutations: {},
	actions: {},
	modules: {},
	dev: {
		// Paths
		assetsSubDirectory: 'static',
		assetsPublicPath: '/',
		// 后端请求地址代理，配置后testIp再之后的页面调用时就直接指代 http://127.0.0.1:8085
		proxyTable: {
			'/api': {
				target: 'http://127.0.0.1:8085',
				changeOrigin: true,
				pathRewrite: {
					'^/api': ''
				}
			}
		},

		// Various Dev Server settings
		host: 'localhost', // can be overwritten by process.env.HOST
		port: 8085, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
		autoOpenBrowser: false,
		errorOverlay: true,
		notifyOnErrors: true,
		poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-

		/**
		 * Source Maps
		 */

		// https://webpack.js.org/configuration/devtool/#development
		devtool: 'cheap-module-eval-source-map',

		// If you have problems debugging vue-files in devtools,
		// set this to false - it *may* help
		// https://vue-loader.vuejs.org/en/options.html#cachebusting
	}

})
