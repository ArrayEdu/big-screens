import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// 路由路径配置
const routes = [{
		path: '/',
		name: 'index',
		component: () => import('../views/index.vue'),
		meta: {
			title: '数据大屏',
			keepAlive: true
		}
	},
	{
		path: '/epidemic-screen',
		name: 'epidemic-screen',
		component: () => import('../views/epidemic-screen/epidemic-screen.vue'),
		meta: {
			title: '肺炎疫情数据大屏',
			keepAlive: true
		}
	}, {
		path: '/equipment-screen',
		name: 'equipment-screen',
		component: () => import('../views/equipment-screen/index.vue'),
		meta: {
			title: '设备管理数据大屏',
			keepAlive: true
		}
	}, {
		path: '/construction-screen',
		name: 'construction-screen',
		component: () => import('../views/construction-screen/index.vue'),
		meta: {
			title: '施工养护综合数据',
			keepAlive: true
		}
	}, {
		path: '/tollStation-screen',
		name: 'tollStation-screen',
		component: () => import('../views/tollStation-screen/index.vue'),
		meta: {
			title: '机电设备数据大屏',
			keepAlive: true
		}
	}
]
const router = new VueRouter({
	mode: "history",
	routes
})

export default router
