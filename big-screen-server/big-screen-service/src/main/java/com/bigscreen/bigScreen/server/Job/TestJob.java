package com.bigscreen.bigScreen.server.Job;

import com.bigscreen.bigScreen.server.Service.IService.IResultService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestJob {

    private static Logger logger = LoggerFactory.getLogger(TestJob.class);

    @Autowired
    private IResultService service;

    @XxlJob("firstJob")
    public ReturnT<String> firstJob(String param) throws Exception {

        logger.info("爬取数据");

        XxlJobLogger.log("Xxl job handler : hello world");

        String result = service.getApiResult();

        XxlJobLogger.log("爬取的结果：{}",result);

        return ReturnT.SUCCESS;
    }

}
