package com.bigscreen.bigScreen.server.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author ZhouWenJie
 * @className ResponseResult
 * @description TODO
 * @date 2020/4/8 14:25
 * @email 18566749129@163.com
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseResult<T> implements Serializable {

    private Boolean state;
    private Integer code;
    private String msg;
    private T data;

    public static <T> ResponseResult<T> succeed() {
        return succeedWith(null, CodeEnum.SUCCESS.getCode(), "success");
    }
    public static <T> ResponseResult<T> succeed(T model) {
        return succeedWith(model, CodeEnum.SUCCESS.getCode(), "");
    }
    public static <T> ResponseResult<T> succeed(Integer code) {
        return succeedWith(null, code, "");
    }
    public static <T> ResponseResult<T> succeed(String msg) {
        return succeedWith(null, CodeEnum.SUCCESS.getCode(), msg);
    }
    public static <T> ResponseResult<T> succeed(T model, Integer code) {
        return succeedWith(model, code, "");
    }
    public static <T> ResponseResult<T> succeed(T model, String msg) {
        return succeedWith(model, CodeEnum.SUCCESS.getCode(), msg);
    }
    public static <T> ResponseResult<T> succeed(Integer code, String msg) {
        return succeedWith(null, code, msg);
    }
    public static <T> ResponseResult<T> succeedWith(T data, Integer code, String msg) {
        return new ResponseResult<>(true, code, msg,data);
    }

    public static <T> ResponseResult<T> failed() {
        return failedWith(null, CodeEnum.ERROR.getCode(), "failed");
    }
    public static <T> ResponseResult<T> failed(T model) {
        return failedWith(model, CodeEnum.ERROR.getCode(), "");
    }
    public static <T> ResponseResult<T> failed(Integer code) {
        return failedWith(null, code, "");
    }
    public static <T> ResponseResult<T> failed(String msg) {
        return failedWith(null, CodeEnum.ERROR.getCode(), msg);
    }
    public static <T> ResponseResult<T> failed(T model, Integer code) {
        return failedWith(model,code, "");
    }
    public static <T> ResponseResult<T> failed(T model, String msg) {
        return failedWith(model, CodeEnum.ERROR.getCode(), msg);
    }
    public static <T> ResponseResult<T> failed(Integer code, String msg) {
        return failedWith(null, code, msg);
    }
    public static <T> ResponseResult<T> failedWith(T data, Integer code, String msg) {
        return new ResponseResult<>(false, code, msg,data);
    }

}
