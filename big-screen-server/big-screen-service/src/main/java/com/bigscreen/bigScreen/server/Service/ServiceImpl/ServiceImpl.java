package com.bigscreen.bigScreen.server.Service.ServiceImpl;

import com.alibaba.fastjson.JSONObject;
import com.bigscreen.bigScreen.server.Service.IService.IResultService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class ServiceImpl implements IResultService {

    @Value("${appid}")
    private String appId;

    @Value("${appsecret}")
    private String appSecret;

    @Value("${yiqingUrl}")
    private String url;

    @Override
    public String getApiResult() {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        StringBuilder sb = new StringBuilder(url);
        sb.append("?");
        sb.append("version=epidemic");
        sb.append("&appid=");
        sb.append(appId);
        sb.append("&");
        sb.append("appsecret=");
        sb.append(appSecret);
        System.out.println(sb.toString());
        HttpGet get = new HttpGet(sb.toString());
        get.setHeader("Content-Type", "application/json;charset=utf8");
        CloseableHttpResponse response = null;
        JSONObject jsonObject = null;
        try {
            response = client.execute(get);
            jsonObject = JSONObject.parseObject(EntityUtils.toString(response.getEntity(),"utf-8"));
            System.out.println(jsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

}
