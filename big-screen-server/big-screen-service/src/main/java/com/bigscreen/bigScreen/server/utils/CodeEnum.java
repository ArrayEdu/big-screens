package com.bigscreen.bigScreen.server.utils;

/**
 * @Author: mall
 */
public enum CodeEnum {
    /** 成功 */
    SUCCESS(200),
    /** 失败 */
    ERROR(500),
    /** RSA错误的密钥*/
    RSA_INVALID_KEY_SPEC(200000001),
    /** RSA错误的算法*/
    RSA_NO_SUCH_ALGORITHM(200000002),
    /** jwt 刷新token过期*/
    JWT_ACCESS_EXPIRED(200000003),
    /** jwt 长token过期*/
    JWT_REFRESH_EXPIRED(200000004),
    /** 错误的jwt*/
    JWT_MALFORMED(200000005),
    /** jwt 错误*/
    JWT_SIGNATURE_ERROR(200000006);

    private Integer code;
    CodeEnum(Integer code){
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
