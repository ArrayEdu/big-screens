package com.bigscreen.bigScreen.server.controller;

import com.bigscreen.bigScreen.server.Service.ServiceImpl.ServiceImpl;
import com.bigscreen.bigScreen.server.utils.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    private ServiceImpl service;

    @RequestMapping("/hello")
    @ResponseBody
    public String sayHello(){
        return "hello";
    }

    @RequestMapping(value = "/httpClientTest", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult<Object> httpClientTest(){
        return ResponseResult.succeed(service.getApiResult());
    }

}
