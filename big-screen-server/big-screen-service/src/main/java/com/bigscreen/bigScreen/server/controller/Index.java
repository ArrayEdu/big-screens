package com.bigscreen.bigScreen.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/index")
public class Index {

    /**
     * 返回一个当前时间
     * 后续可扩展为返回数据
     * @param response
     * @return
     */
    @RequestMapping(value = "/date", method = RequestMethod.GET)
    @ResponseBody
    public ResponseResult<String> getdate(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss" );
        return ResponseResult.succeed(sdf.format(new Date().getTime()),"返回成功");
    }


}
