## 一、项目描述

- 前端用的是vue + DataV + echarts进行的开发。
- 后端服务的架构为spring boot + xxl-jod定时任务处理来搭建整个服务，数据库用的是mysql。
- 整合了四个类型的数据大屏，重新构建了主页，可以通过主页来选择不同类型的数据大屏进行查看。
- 项目需要全屏展示（按 F11）。
- 项目部分区域使用了全局注册方式，增加了打包体积，在实际运用中请使用**按需引入**。
- 项目环境：vue-cli-3.0、webpack-4.0、npm-6.13、node-v12.16、java-1.8。
- 在本项目上做进一步的改造需要具备一些vue的基础，比如组件，路由，基本语法等。
- 演示地址：http://www.pwriter.top:8082/ 未启动后端项目。

友情链接：

1.  [DataV 官方文档（建议使用之前先浏览）](http://datav.jiaminghi.com/guide/)
2.  [echarts 实例](https://echarts.apache.org)，[echarts 官方文档-特性](https://echarts.apache.org/zh/feature.html)
3.  [Vue 官方文档](https://cn.vuejs.org/v2/guide/instance.html)
4.  [xxl-job官方地址](https://www.xuxueli.com/index.html)

项目展示
![首页](https://images.gitee.com/uploads/images/2020/0806/093342_ed5cbbf4_464076.png "屏幕截图.png")
![项目展示1](https://images.gitee.com/uploads/images/2020/0411/221307_0f8af2e7_4964818.gif "20200411_221020.gif")
![项目展示2](https://images.gitee.com/uploads/images/2020/0806/093427_1dffa778_464076.png "屏幕截图.png")
![项目展示3](https://images.gitee.com/uploads/images/2020/0806/093454_26c2cb7d_464076.png "屏幕截图.png")
![项目展示4](https://images.gitee.com/uploads/images/2020/0806/093518_05d80ef0_464076.png "屏幕截图.png")
## 二、项目结构介绍
![结构图](https://images.gitee.com/uploads/images/2020/0630/100524_23ff6b88_464076.png "屏幕截图.png")
![流程图](https://images.gitee.com/uploads/images/2021/0727/113228_9fddbbb5_464076.png "数据报表.png")

## 三、使用说明

1. **如何启动前端项目**
   - 需要提前安装好`nodejs`与`npm`,下载项目后在项目主目录下运行`npm/cnpm install`拉取依赖包，然后使用 `vue-cli` 或者直接使用命令`npm run serve`，就可以启动项目，启动项目后需要手动全屏（按 F11）。

2. **如何启动后端项目**
   - 后端项目使用了xxl-job做一个定时任务处理，当然，你也可以使用spring boot内置的定时处理，如**@Scheduled**注解，需要更改big-screen后端的xxl-job服务地址在下图的配置文件中做修改
![xxl-job服务地址配置](https://images.gitee.com/uploads/images/2020/0630/095608_bcc3554b_464076.png "屏幕截图.png")
   - 项目中集成了xxl-job的源码，先启动xxl-job-admin，注意需要本地maven install xxl-job-core，在启动后端服务，xxl-job的详细说明，请访问作者的[**官方文档**](https://www.xuxueli.com/xxl-job/)

3. **如何动态渲染图表**
   - 在`components/echart`下的文件，比如`drawPie()`是渲染函数，`echartData`是需要动态渲染的数据，当外界通过`props`传入新数据，我们可以使用`watch()`方法去监听，一但数据变化就调用`this.drawPie()`并触发内部的`.setOption`函数，重新渲染一次图表。

```js
//这里是子组件内部

props: ["listData"],
watch: {
  listData(newValue) {
     this.echartData= newValue;
     this.drawPie();
   },
  },
methods: {
  drawPie() {
  	.....
  	'渲染节点名称'.setOption(option);
  }
 }
```

4. **跨域配置**
    - 一般我们后端和前端的服务器地址是不一样的，在前端调用后端接口的时候会存在跨域的问题，项目中的跨域配置在 **store/vue-store.js** 中，配置项做了详细说明，如果需更多配置项，请查阅[**vue官网信息-路由**](https://cn.vuejs.org/v2/guide/routing.html)

```
export default new Vuex.Store({
	state: {},
	mutations: {},
	actions: {},
	modules: {},
	dev: {
		// Paths
		assetsSubDirectory: 'static',
		assetsPublicPath: '/',
		// 后端请求地址代理，配置后testIp再之后的页面调用时就直接指代 http://127.0.0.1:8085
		proxyTable: {
			'/api': {
				target: 'http://127.0.0.1:8085',
				changeOrigin: true,
				pathRewrite: {
					'^/api': ''
				}
			}
		},
		// Various Dev Server settings
		host: 'localhost', // can be overwritten by process.env.HOST
		port: 8085, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
		autoOpenBrowser: false,
		errorOverlay: true,
		notifyOnErrors: true,
		poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-
		devtool: 'cheap-module-eval-source-map',
	}

})
```

5. **前端接口配置**
- 为了方便管理调用的接口，抽取了一个配置类，如下图
![接口配置文件](https://images.gitee.com/uploads/images/2020/0702/155424_73d66b66_464076.png "屏幕截图.png")
```
let resquest = "/api";
```
对应vue-store.js文件中的proxyTable配置中的/api，会映射对应的后端接口路径
![跨域配置](https://images.gitee.com/uploads/images/2020/0702/155645_baa3e1a9_464076.png "屏幕截图.png")

- 目前只在epidemic-screen.vue中与后端对接了一个接口，后续可以根据自己的需求扩展。

```
methods: {
			cancelLoading() {
				setTimeout(() => {
					this.loading = false;
				}, 2000);
			},

			// 调用接口获取数据
			fetchList() {
				getListAPI().then(res => {
					if(res != undefined)
					{
						if( res.data.code == '200' ){
							this.nowTime = res.data.data;
							console.log(res.data);
						}else{
							clearInterval(this.timer);
						}
					}else{
						alert(res);
						clearInterval(this.timer);
					}
				});
				// this.timer = setInterval(this.fetchList, 10000);
			}
		}

```

6. **如何更换边框**
   - 边框是使用了 DataV 自带的组件，如：

```html
<dv-border-box-1></dv-border-box-1>
<dv-border-box-2></dv-border-box-2>
...
```

只需要去 views 目录下去寻找对应的位置去查找并替换就可以，具体的种类请去 DavaV 官网查看


7. **如何更换图表**
   - 直接进入 `components/echart` 下的文件修改成你要的 echarts 模样，可以去[echarts 官方社区](https://gallery.echartsjs.com/explore.html#sort=rank~timeframe=all~author=all)里面查看案例。
